## OSM20L_Projekt2

**Przedmiot:** OSM

**Projekt:** 2

**Zadanie:** 3

**Temat:** Przeglądarka obrazów medycznych zapisanych w formacie DICOM. Aplikacja powinna
pozwalać na jednoczesne otwarcie wielu plików, grupowanie zawartych w nich obrazów w oparciu o
identyfikatory Patient ID, Study Instance UID i Series Instance UID oraz wybranie obrazu do
wyświetlenia (do grupowania i wyboru mozna np. użyć drzewa klasy JTree). Rozmiar wyświetlanego
obrazu musi dostosowywać się do wielkości okna aplikacji. Ponadto użytkownik powinien mieć
możliwość przybliżania wybranych fragmentów obrazu i zmianę parametrów jego wyświetlania
(szerokości i położenia środka okna kontrastu). Wraz z obrazem należy wyświetlać podstawowe
informacje z nagłówka DICOM dotyczące pacjenta i badania.


**Zespół:** Krzysztof Wyszyński, Kamil Wojtczyk

**Biblioteki:** JDK-11.0.2,JavaFX, dcm4che

**Uwagi dodatkowe:** Instrukcja zamieszczona w oddzielnym pliku PDF w katalogu głównym 