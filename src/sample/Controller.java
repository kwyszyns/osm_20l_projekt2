package sample;


import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.TreeItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import org.dcm4che2.data.Tag;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.RescaleOp;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa odpowiadająca za interakcję interfejsu graficznego z działaniami użytkownika
 */
public class Controller {

    private GUICreator guiCreator;
    private static Controller controller;
    private DicomReader dicomReader;
    private List<File> list, fileList;
    private TreeItem<String> patientID, studyInstanceUID, seriesInstanceUID, file, file1, patientName, studyInstance, seriesInstance;
    private TreeItem<String> root, secondRoot;
    private List<TreeItem<String>> seriesInstanceList, seriesInstanceUIDlist;
    private String fileName;
    private Rectangle2D viewport;
    private double width, height, widthScaleFactor, heightScaleFactor;
    private BufferedImageOp op;
    private Boolean dicomShown;
    private int clickCounter = 0;


    /**
     * Konstruktor tworzący listę plików i pacjentów oraz wywołujący instancję GUI - dzięki temu może korzystać z metod i pól interfejsu graficznego
     */
    private Controller() {
        guiCreator = GUICreator.getInstance();
        fileList = new ArrayList<>();
        seriesInstanceList = new ArrayList();
        seriesInstanceUIDlist = new ArrayList<>();
        root = new TreeItem<>("DICOM");
        secondRoot = new TreeItem<>("DICOM");


    }

    /**
     * Klasyczna metoda identyfikująca Controller jako Singleton
     *
     * @return
     */

    public static Controller getInstance() {
        if (controller == null) {
            controller = new Controller();
        }
        return controller;
    }


    /**
     * Metoda wczytująca wybrane pliki DICOM i porządkujące je według PatientID,StudyInstanceUID,SeriesInstanceUID
     * Stwórz listę wybranych plików
     * Przeszukaj listę plików i dla każdego pliku:
     * Stwórz pacjenta.Jeśli lista pacjentów jest pusta, wyświetl pacjenta w drzewie wyboru, dodaj go do listy pacjentów oraz dodaj listę wybranych plików do listy wszystkich plików.
     * Jeśli lista pacjentów nie jest pusta, porównuj kolejno PatientID,StudyInstanceUID,SeriesInstanceUID pacjenta z elementami, które już znajdują się w drzewie wyboru.
     * Na podstawie porównań uporządkuj pliki według ustalonej hierarchii.
     */

    public void selectFile() {

        guiCreator.setFileChooser();
        list = guiCreator.openFileChooser();
        if (list != null) {

            for (int i = 0; i < list.size(); i++) {

                guiCreator.setSelectedFile(list.get(i));
                Patient patient = new Patient(guiCreator.getSelectedFile());
                if (patient.getPatientID() != null) {

                    if (root.getChildren().isEmpty()) {

                        addPatientToTree(patient);


                    } else {

                        if (hasTheSamePatientID(patient)) {
                            if (hasTheSameStudyInstanceUID(patient)) {
                                if (hasTheSameSeriesInstanceUID(patient)) {
                                    setFile(patient.getFileName());

                                    if (!hasTheSameFile(patient)) {
                                        addFileToTree();

                                    }


                                } else {

                                    setSeriesInstanceUID(patient.getSeriesInstanceUID());
                                    setSeriesInstance(patient.getSeriesInstance());
                                    setFile(patient.getFileName());
                                    addSeriesInstanceToTree();
                                    addFileToTree();


                                }
                            } else {
                                setStudyInstanceUID(patient.getStudyInstanceUID());
                                setStudyInstance(patient.getStudyInstance());
                                setSeriesInstanceUID(patient.getSeriesInstanceUID());
                                setSeriesInstance(patient.getSeriesInstance());
                                setFile(patient.getFileName());
                                addStudyInstanceToTree();
                                addSeriesInstanceToTree();
                                addFileToTree();


                            }
                        } else {

                            addPatientToTree(patient);
                        }


                    }
                    if (!fileList.contains(list.get(i))) {
                        fileList.add(list.get(i));
                    }


                }

                guiCreator.setRoot(secondRoot);


            }
        }
    }


    public void addSeriesInstanceToTree() {
        studyInstanceUID.getChildren().add(seriesInstanceUID);
        studyInstance.getChildren().add(seriesInstance);
    }

    public void addStudyInstanceToTree() {
        patientID.getChildren().add(studyInstanceUID);
        patientName.getChildren().add(studyInstance);
    }

    public void addFileToTree() {
        seriesInstanceUID.getChildren().add(file);
        seriesInstance.getChildren().add(file1);
    }


    public void setStudyInstanceUID(String x) {

        studyInstanceUID = new TreeItem<>(x);

    }

    public void setStudyInstance(String x) {
        studyInstance = new TreeItem<>(x);

    }

    public void setPatientName(String x) {
        patientName = new TreeItem<>(x);

    }

    public void setSeriesInstance(String x) {
        seriesInstance = new TreeItem<>(x);
        seriesInstanceList.add(seriesInstance);

    }

    public void setPatientID(String x) {

        patientID = new TreeItem<>();
        patientID.setValue(x);


    }

    public void addTreeItems() {
        secondRoot.getChildren().add(patientName);
        patientName.getChildren().add(studyInstance);
        studyInstance.getChildren().add(seriesInstance);
        seriesInstance.getChildren().add(file1);

        root.getChildren().add(patientID);
        patientID.getChildren().add(studyInstanceUID);
        studyInstanceUID.getChildren().add(seriesInstanceUID);
        seriesInstanceUID.getChildren().add(file);
    }

    public void addPatientToTree(Patient patient) {
        setPatientID(patient.getPatientID());
        setPatientName(patient.getPatientName());
        setStudyInstance(patient.getStudyInstance());
        setSeriesInstance(patient.getSeriesInstance());
        setStudyInstanceUID(patient.getStudyInstanceUID());
        setSeriesInstanceUID(patient.getSeriesInstanceUID());
        setFile(patient.getFileName());
        addTreeItems();
    }

    public void setFile(String filex) {
        file = new TreeItem<>();
        file.setValue(filex);

        file1 = new TreeItem<>();
        file1.setValue(filex);
    }


    public void setSeriesInstanceUID(String value) {
        seriesInstanceUID = new TreeItem<>(value);
        seriesInstanceUIDlist.add(seriesInstanceUID);


    }

    /**
     * Metoda sprawdzająca czy plik wybranego pacjenta jest już w drzewie wyboru
     *
     * @param patient
     * @return
     */
    public boolean hasTheSameFile(Patient patient) {
        Boolean bool = false;
        for (int i = 0; i < seriesInstanceUID.getChildren().size(); i++) {
            fileName = seriesInstanceUID.getChildren().get(i).getValue();
            if (fileName.equals(patient.getFileName())) {
                bool = true;
                break;
            }
        }
        return bool;
    }

    /**
     * Metoda sprawdzająca czy SeriesInstanceUID wybranego pacjenta jest już w drzewie wyboru
     *
     * @param patient
     * @return
     */

    public boolean hasTheSameSeriesInstanceUID(Patient patient) {
        Boolean bool = false;
        for (int i = 0; i < studyInstanceUID.getChildren().size(); i++) {
            seriesInstanceUID = studyInstanceUID.getChildren().get(i);
            seriesInstance = studyInstance.getChildren().get(i);
            if (seriesInstanceUID.getValue().equals(patient.getSeriesInstanceUID())) {
                bool = true;
                break;
            }
        }
        return bool;
    }


    /**
     * Metoda sprawdzająca czy StudyInstanceUID wybranego pacjenta jest już w drzewie wyboru
     *
     * @param patient
     * @return
     */

    public boolean hasTheSameStudyInstanceUID(Patient patient) {
        Boolean bool = false;
        for (int i = 0; i < patientID.getChildren().size(); i++) {
            studyInstanceUID = patientID.getChildren().get(i);
            studyInstance = patientName.getChildren().get(i);
            if (studyInstanceUID.getValue().equals(patient.getStudyInstanceUID())) {
                bool = true;
                break;
            }
        }
        return bool;
    }

    /**
     * Metoda sprawdzająca czy PatientID wybranego pacjenta jest już w drzewie wyboru
     *
     * @param patient
     * @return
     */

    public boolean hasTheSamePatientID(Patient patient) {
        Boolean bool = false;
        for (int i = 0; i < root.getChildren().size(); i++) {
            patientID = root.getChildren().get(i);
            patientName = secondRoot.getChildren().get(i);
            if (patientID.getValue().equals(patient.getPatientID())) {
                bool = true;
                break;

            }
        }
        return bool;
    }

    /**
     * Metoda przerysowująca oryginalny obraz do obrazu o innych, wybranych rozmiarach
     */
    public void scaleImage() {

        BufferedImage oldImage = dicomReader.getImg();
        if (guiCreator.getPaneHeight() > 0 && guiCreator.getPaneWidth() > 0) {
            BufferedImage scaledImage = new BufferedImage(guiCreator.getPaneWidth(), guiCreator.getPaneHeight(), oldImage.getType());
            Graphics2D g2 = scaledImage.createGraphics();
            g2.drawImage(oldImage, 0, 0, guiCreator.getPaneWidth(), guiCreator.getPaneHeight(), null);
            g2.dispose();
            guiCreator.setImage(SwingFXUtils.toFXImage(scaledImage, null));
        }

    }


    /**
     * Metoda przybliżająca wybrany fragment obrazu. Określa położenie początkowego punktu przybliżanego fragmentu poprzez stosunek szerokości i wysokości okna zaznaczonego do wysokości i szerokości okna, w którym dokonano zaznaczenia.
     *
     * @param mouseEvent
     */
    public void scale(MouseEvent mouseEvent) {

        width = guiCreator.getZoomWidth();
        height = guiCreator.getZoomHeight();
        if (viewport == null) {

            widthScaleFactor = width / guiCreator.getPaneWidth();
            heightScaleFactor = height / guiCreator.getPaneHeight();
            if (width > 1 || height > 1) {
                viewport = new Rectangle2D(guiCreator.getZoomX(), guiCreator.getZoomY(), width, height);
            }
        } else {
            viewport = new Rectangle2D(viewport.getMinX() + guiCreator.getZoomX() * widthScaleFactor, viewport.getMinY() + guiCreator.getZoomY() * heightScaleFactor, guiCreator.getZoomWidth() * widthScaleFactor, guiCreator.getZoomHeight() * heightScaleFactor);
            widthScaleFactor = width * widthScaleFactor / guiCreator.getPaneWidth();
            heightScaleFactor = height * heightScaleFactor / guiCreator.getPaneHeight();
        }

        guiCreator.setViewport(viewport);

    }

    /**
     * Metoda zwracająca index wybranej gałęzi seriesInstance
     *
     * @param item
     * @return
     */

    public int getSeriesInstanceIndex(TreeItem<String> item) {
        int i;
        for (i = 0; i < seriesInstanceList.size(); i++) {
            if (seriesInstanceList.get(i).getValue().equals(item.getValue())) {
                break;
            }
        }
        return i;
    }

    /**
     * Metoda zwracająca index pliku wybranego do usunięcia
     *
     * @param item
     * @param t1
     * @return
     */
    public int getSelectedFileIndex(TreeItem<String> item, TreeItem<String> t1) {
        int i;
        for (i = 0; i < seriesInstanceList.get(getSeriesInstanceIndex(item)).getChildren().size(); i++) {
            if (t1.getValue().equals(seriesInstanceList.get(getSeriesInstanceIndex(item)).getChildren().get(i).getValue())) {
                break;
            }
        }
        return i;
    }


    /**
     * Metoda zmieniająca kontrast/jasność wyświetlonego obrazu
     *
     * @param scrollEvent
     */

    public void changeGreyScale(ScrollEvent scrollEvent) {
        if (clickCounter % 2 == 0) {
            if (scrollEvent.getDeltaY() > 0) {
                op = new RescaleOp(1, 5, null);
            } else if (scrollEvent.getDeltaY() < 0) {
                op = new RescaleOp(1, -5, null);
            }

        } else {
            if (scrollEvent.getDeltaY() > 0) {
                op = new RescaleOp(1.5f, 0, null);
            } else if (scrollEvent.getDeltaY() < 0) {
                op = new RescaleOp(2 / 3f, 0, null);
            }
        }

        BufferedImage dest = op.filter(SwingFXUtils.fromFXImage(guiCreator.getImage(), null), null);
        guiCreator.setImage(SwingFXUtils.toFXImage(dest, null));
    }


    /**
     * Metoda przywracająca oryginalny obraz rozmiaru
     */
    public void zoomOut() {
        viewport = null;
        guiCreator.setViewport(null);
    }

    /**
     * Metoda pokazująca podstawowe dane pacjenta i badania na ekranie
     */
    public void showPatientInformation() {
        guiCreator.setNameLabel(dicomReader.getDicomObject().getString(Tag.PatientName));
        guiCreator.setDateLabel(dicomReader.getDicomObject().getString(Tag.StudyDate));
        guiCreator.setModalityLabel(dicomReader.getDicomObject().getString(Tag.Modality));

    }

    public void addClick() {
        clickCounter++;
    }

    public void deleteFile(TreeItem<String> t1) {

        if (dicomShown) {
            if (fileList != null) {
                for (int i = 0; i < fileList.size(); i++) {
                    if (t1.getValue().equals(fileList.get(i).getName())) {
                        seriesInstance = guiCreator.getItem();
                        seriesInstanceUID = seriesInstanceUIDlist.get(getSeriesInstanceIndex(seriesInstance));
                        seriesInstanceUID.getChildren().remove(getSelectedFileIndex(seriesInstance, t1));
                        guiCreator.clearImageView();
                        seriesInstance.getChildren().remove(getSelectedFileIndex(seriesInstance, t1));
                        fileList.remove(i);
                        break;
                    }

                }
            }


        }


    }


    /**
     * Metoda wyświetlająca wybrany obraz DICOM
     *
     * @param t1
     */

    public void showDicom(TreeItem<String> t1) {
        dicomShown = false;

        if (!fileList.isEmpty()) {
            for (int i = 0; i < fileList.size(); i++) {
                if (t1.getValue().equals(fileList.get(i).getName())) {
                    guiCreator.showImageView();
                    dicomReader = new DicomReader(fileList.get(i));
                    dicomReader.run();
                    guiCreator.setImage(SwingFXUtils.toFXImage(dicomReader.getImg(), null));
                    scaleImage();
                    zoomOut();
                    showPatientInformation();
                    dicomShown = true;
                }
            }
        }


    }
}
