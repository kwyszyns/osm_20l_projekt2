package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;


/**
 * Klasa tworząca interfejs graficzny aplikacji
 */
public class GUICreator {


    private static GUICreator guiCreator;
    private MenuBar menuBar;
    private Menu menu;
    private MenuItem menuItem;
    private ScrollPane scrollPane;
    private AnchorPane pane, pane1, root;
    private Label nameLabel, dateLabel, modalityLabel, xPosition, yPosition;
    private ImageView imageView;
    private Rectangle rectangle;
    private File selectedFile;
    private Scene scene;
    private TreeItem<String> seriesInstance, file, patientID, studyInstance;
    private TreeView<String> treeView;
    private FileChooser fileChooser;
    private TreeItem rootItem;


    private GUICreator() {
        createPanes();
        setPane(200, 600, 0, 25, pane);
        setPane(600, 600, 200, 25, pane1);
        createMenu();
        setTreeView();
        setImageView();
        createCursorCoordinates();
        createRectangle();
        setPatientInformationFields();
        scrollPane.setContent(treeView);


    }


    public static GUICreator getInstance() {
        if (guiCreator == null) {
            guiCreator = new GUICreator();
        }
        return guiCreator;
    }


    public void showCursorPosition(double x, double y) {
        xPosition.setText(String.valueOf(x));
        yPosition.setText(String.valueOf(y));
    }


    public void setPatientInformationFields() {
        nameLabel = new Label();
        dateLabel = new Label();
        modalityLabel = new Label();
        pane1.getChildren().add(nameLabel);
        pane1.getChildren().add(dateLabel);
        pane1.getChildren().add(modalityLabel);
        nameLabel.setLayoutX(20);
        dateLabel.setLayoutX(20);
        modalityLabel.setLayoutX(20);
        nameLabel.setLayoutY(20);
        dateLabel.setLayoutY(30);
        modalityLabel.setLayoutY(40);
        nameLabel.setTextFill(Color.WHITE);
        dateLabel.setTextFill(Color.WHITE);
        modalityLabel.setTextFill(Color.WHITE);

    }

    public void setDateLabel(String date) {
        dateLabel.setText(date);
    }

    public void setModalityLabel(String modality) {
        modalityLabel.setText(modality);
    }

    public void setNameLabel(String name) {
        nameLabel.setText(name);
    }

    public void setFile(String filex) {
        file = new TreeItem<>();
        file.setValue(filex);
    }


    public void addTreeItems() {
        rootItem.getChildren().add(patientID);
        patientID.getChildren().add(studyInstance);
        studyInstance.getChildren().add(seriesInstance);
        seriesInstance.getChildren().add(file);
    }

    public TreeItem<String> getRootItem() {
        return rootItem;
    }

    public List openFileChooser() {
        return fileChooser.showOpenMultipleDialog(null);
    }

    public void addPatientToTree(Patient patient) {
        setPatientID(patient.getPatientName());
        setStudyInstance(patient.getStudyInstance());
        setSeriesInstance(patient.getSeriesInstance());
        setFile(patient.getFileName());
        addTreeItems();
    }


    public void addFileToTree() {
        seriesInstance.getChildren().add(file);
    }


    public TreeItem getStudyInstance() {
        return studyInstance;
    }


    public TreeItem getSeriesInstance() {
        return seriesInstance;
    }

    public void setSeriesInstance(String x) {
        seriesInstance = new TreeItem<>();
        seriesInstance.setValue(x);
//        seriesInstanceList.add(seriesInstance);

    }


    public void setPatientID(String x) {

        patientID = new TreeItem<>();
        patientID.setValue(x);
    }

    public void setStudyInstance(String x) {

        studyInstance = new TreeItem<>();
        studyInstance.setValue(x);
    }


    public void setViewport(Rectangle2D viewport) {
        imageView.setViewport(viewport);
    }

    public double getZoomWidth() {
        return rectangle.getBoundsInParent().getWidth();
    }

    public double getZoomHeight() {
        return rectangle.getBoundsInParent().getHeight();
    }

    public double getZoomX() {
        return rectangle.getBoundsInParent().getMinX();
    }

    public double getZoomY() {
        return rectangle.getBoundsInParent().getMinY();
    }

    public void addSeriesInstanceToTree() {
        studyInstance.getChildren().add(seriesInstance);
    }

    public void addStudyInstanceToTree() {
        patientID.getChildren().add(studyInstance);
    }

    public File getSelectedFile() {
        return selectedFile;
    }

    public void setSelectedFile(File selectedFile) {
        this.selectedFile = selectedFile;
    }


    public Image getImage() {
        return imageView.getImage();
    }


    public void setTreeView() {
        treeView = new TreeView<>();
        rootItem = new TreeItem("DICOM");
        treeView.setRoot(rootItem);
        treeView.setPrefHeight(pane.getPrefHeight());
        System.out.println(treeView.getHeight());
        treeView.setPrefWidth(200);
        pane.getChildren().add(treeView);
        treeView.setVisible(false);


    }

    public void setRoot(TreeItem root) {
        treeView.setRoot(root);
    }


    public void setImage(Image image) {
        imageView.setImage(image);
    }

    public void setImageView() {

        imageView = new ImageView();

        pane1.getChildren().add(imageView);
        imageView.setPreserveRatio(false);
        imageView.setCache(true);
        imageView.setSmooth(true);


        imageView.fitWidthProperty().bind(pane1.prefWidthProperty());
        imageView.fitHeightProperty().bind(pane1.prefHeightProperty());


    }

    public void clearImageView() {
        imageView.setVisible(false);
        //treeView.getSelectionModel().clearSelection();
    }

    public void showImageView() {
        imageView.setVisible(true);
    }


    public void createScene(Stage primaryStage) {

        scene = new Scene(root, 800, 625);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("DICOM Browser");
        adjustPanesToScene();


    }

    public void adjustPanesToScene() {

        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                pane1.setPrefHeight((double) t1 - 25);
                pane.setPrefHeight((double) t1 - 25);
                treeView.setPrefHeight(pane.getPrefHeight());

            }
        });
        pane1.setPrefWidth(scene.getWidth() - 200);
        menuBar.prefWidthProperty().bind(scene.widthProperty());


    }

    public AnchorPane getPane() {
        return pane;
    }

    public ScrollPane getScrollPane() {
        return scrollPane;
    }

    public void createCursorCoordinates() {
        xPosition = new Label();
        xPosition.setTextFill(Color.WHITE);

        yPosition = new Label();
        yPosition.setTextFill(Color.WHITE);
        xPosition.setLayoutX(500);
        xPosition.setLayoutY(20);
        yPosition.setLayoutX(500);
        yPosition.setLayoutY(30);
        pane1.getChildren().add(xPosition);
        pane1.getChildren().add(yPosition);

    }

    public void createPanes() {
        root = new AnchorPane();
        pane = new AnchorPane();
        pane1 = new AnchorPane();
        scrollPane = new ScrollPane();


    }

    public Integer getPaneWidth() {
        return (int) pane1.getPrefWidth();
    }

    public Integer getPaneHeight() {
        return (int) pane1.getPrefHeight();
    }

    public void setPane(int width, int height, int layoutX, int layoutY, AnchorPane pane) {
        pane.setPrefWidth(width);
        pane.setPrefHeight(height);
        pane.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        pane.setLayoutX(layoutX);
        pane.setLayoutY(layoutY);
        root.getChildren().add(pane);

    }

    public void createMenu() {
        menuBar = new MenuBar();
        menuBar.setPrefWidth(800);
        menuBar.setPrefHeight(25);
        menu = new Menu("File");
        menuBar.getMenus().add(menu);
        root.getChildren().add(menuBar);
        menuItem = new MenuItem("Load");
        menu.getItems().add(menuItem);

    }

    public void setFileChooser() {
        fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));


    }

    public void createRectangle() {
        rectangle = new Rectangle();
        rectangle.setWidth(0);
        rectangle.setHeight(0);
        rectangle.setStroke(Color.BLUE);
        rectangle.setStrokeLineCap(StrokeLineCap.ROUND);
        rectangle.setFill(Color.LIGHTBLUE.deriveColor(0, 1.2, 1, 0.6));

        rectangle.setStrokeWidth(1);
        pane1.getChildren().add(rectangle);
    }


    public void setZoomingRectangle(MouseEvent mouseEvent) {


        rectangle.setWidth(0);
        rectangle.setHeight(0);
        rectangle.setX(mouseEvent.getX());
        rectangle.setY(mouseEvent.getY());
        rectangle.setVisible(false);


    }

    public int getSelectedIndex() {
        return treeView.getSelectionModel().getSelectedIndex();
    }

    public void showTree() {
        treeView.setVisible(true);
    }

    public void clearZoom() {
        rectangle.setVisible(false);
    }

    public void drawZoom(MouseEvent mouseDragEvent) {

        rectangle.setVisible(true);
        rectangle.setWidth(mouseDragEvent.getX() - rectangle.getX());
        rectangle.setHeight(mouseDragEvent.getY() - rectangle.getY());


    }


    public void adjustCoordinatesToPane() {
        xPosition.setLayoutX(pane1.getPrefWidth() - 100);
        yPosition.setLayoutX(pane1.getPrefWidth() - 100);
    }

    public TreeItem<String> getItem() {
        return treeView.getSelectionModel().getSelectedItem().getParent();
    }


    public void setPopUpWindow() {
        Stage stage = new Stage();
        AnchorPane popUpPane = new AnchorPane();
        popUpPane.setPrefHeight(200);
        popUpPane.setPrefWidth(200);
        Label label = new Label("Failed to read DICOM file. Please choose correct file.");
        label.setLayoutY(70);
        popUpPane.getChildren().add(label);
        Scene scene = new Scene(popUpPane);
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
        stage.setResizable(false);


    }


    public void setControls(Controller controller) {

        menuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                showTree();
                controller.selectFile();
            }
        });


        treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<String>>() {
            @Override
            public void changed(ObservableValue<? extends TreeItem<String>> observableValue, TreeItem<String> stringTreeItem, TreeItem<String> t1) {
                controller.showDicom(t1);
                System.out.println(treeView.getSelectionModel().getSelectedIndex());
                seriesInstance = treeView.getTreeItem(4);


            }
        });

        treeView.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode().equals(KeyCode.DELETE)) {
                    controller.deleteFile(treeView.getSelectionModel().getSelectedItem());

                }
            }
        });


        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                adjustPanesToScene();
                adjustCoordinatesToPane();


            }
        });


        pane1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                showCursorPosition(mouseEvent.getX(), mouseEvent.getY());

            }
        });


        if (imageView != null) {

            imageView.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    setZoomingRectangle(mouseEvent);
                    if (mouseEvent.getButton() == MouseButton.MIDDLE) {

                        controller.addClick();
                    }


                }
            });


            imageView.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseDragEvent) {
                    if (mouseDragEvent.getButton() == MouseButton.PRIMARY) {
                        drawZoom(mouseDragEvent);
                    }
                }
            });

            imageView.setOnMouseReleased(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (mouseEvent.getButton() == MouseButton.PRIMARY) {
                        controller.scaleImage();
                        controller.scale(mouseEvent);
                        clearZoom();
                    } else if (mouseEvent.getButton() == MouseButton.SECONDARY) {
                        controller.zoomOut();
                        controller.scaleImage();

                    }
                }
            });


            imageView.setOnScroll(new EventHandler<ScrollEvent>() {
                @Override
                public void handle(ScrollEvent scrollEvent) {
                    controller.changeGreyScale(scrollEvent);


                }
            });
        }


    }


}
