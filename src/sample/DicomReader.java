package sample;


import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.util.CloseUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


/**
 * Klasa odczytująca informacje dotyczące badania i pacjenta z pliku DICOM
 */

public class DicomReader implements Runnable {

    private DicomInputStream dis;
    private DicomObject dcm;
    private BufferedImage img;
    private File file;


    public DicomReader(File file) {
        this.file = file;
    }

    public BufferedImage getImg() {
        return img;
    }


    public BufferedImage readImg(File file) {
        try {
            img = ImageIO.read(file);


        } catch (IOException ie) {
            GUICreator.getInstance().setPopUpWindow();

        }
        return img;
    }

    public void run() {

        dis = null;
        dcm = null;
        img = null;

        try {
            dis = new DicomInputStream(file);
            dcm = dis.readDicomObject();
            img = readImg(file);


        } catch (IOException ie) {
            GUICreator.getInstance().setPopUpWindow();

        } finally {
            if (dis != null) {
                CloseUtils.safeClose(dis);
            }
        }

    }


    public DicomObject getDicomObject() {
        return dcm;
    }


}