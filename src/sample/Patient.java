package sample;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import java.io.File;

/**
 * Klasa tworząca pacjenta i przypisująca mu wartości odczytane z pliku DICOM
 */
public class Patient {


    public String getPatientID() {
        return patientID;
    }

    public String getStudyInstanceUID() {
        return studyInstanceUID;
    }

    public String getSeriesInstanceUID() {
        return seriesInstanceUID;
    }

    private String patientID;
    private String studyInstanceUID;
    private String seriesInstanceUID;

    public String getSeriesInstance() {
        return seriesInstance;
    }

    private String seriesInstance;

    public String getStudyInstance() {
        return studyInstance;
    }

    public String getPatientName() {
        return patientName;
    }

    private String studyInstance;
    private String patientName;

    public String getFileName() {
        return fileName;
    }

    private String fileName;

    public Patient(File file) {

        DicomReader dicomReader = new DicomReader(file);
        dicomReader.run();
        DicomObject dcm = dicomReader.getDicomObject();
        if (dcm != null) {
            patientID = dcm.getString(Tag.PatientID);
            studyInstanceUID = dcm.getString(Tag.StudyInstanceUID);
            seriesInstanceUID = dcm.getString(Tag.SeriesInstanceUID);
            seriesInstance = dcm.getString(Tag.SeriesDescription);
            studyInstance = dcm.getString(Tag.StudyDescription);
            patientName = dcm.getString(Tag.PatientName);
            fileName = file.getName();
        }
    }

}
