module OSM2 {

    requires dcm4che.audit;
    requires dcm4che.base64;
    requires dcm4che.core;
    requires dcm4che.filecache;
    requires dcm4che.hp;
    requires dcm4che.image;
    requires dcm4che.imageio;
    requires dcm4che.imageio.rle;
    requires dcm4che.iod;
    requires dcm4che.net;
    requires dcm4che.soundex;
    requires jai.imageio;
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.media;
    requires java.sql;
    requires java.desktop;
    requires javafx.swing;
    opens sample;
}